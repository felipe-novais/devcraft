# Indice

<!--ts-->
   * [Sobre](#-sobre)
   * [Níveis](#-níveis)
   * [Ferramentas Utilizadas](#-ferramentas-utilizadas)
   * [Como jogar no servidor](#-como-jogar-no-servidor)
      * [Instalação](#instalacoa)
      * [Entrando no Servidor](#entrando-no-servidor)
<!--te-->


# 📝 Sobre

DevCraft é um servidor dedicado á prática e aprendizado de programação em LUA dentro do jogo "minecraft". Que tem como principal objetivo ensinar de uma maneira mais interativa e divertida.

# ✅ Níveis

O jogo se passa em uma ilha flutuante com ambiente medieval com vários desafios de programação, armadilhas e muita diversão.
Segue abaixo a lista dos níveis do servidor
- [Labirinto]
- [O chão é lava]
- [Desafio das armaduras]
- [Desafio final]

# ✅ Ferramentas Utilizadas

- [Tlauncher](https://tlauncher.org/en/)
- [Computercraft](https://www.computercraft.info/)
- [Minecraft](https://www.minecraft.net/pt-pt)
- [Java](https://www.java.com/pt-BR/download/)


# 💾 Como jogar no servidor
   

## Instalação
Antes de tudo, tenha certeza que o java está instalado em sua máquina.<br />
Baixe e instale o client "TLauncher" de acordo com seu sistema operacional.<br />
Com o Launcher Aberto, selecionem a versão "Forge 1.8.9" e clique no botão "instalação"<br />
Agora clique no botão com ícone de pasta ao lado do botão "Entrar no jogo"<br />
<h1 display="inline">
  <img src="server/install/instalacao1.png">
</h1>
Será aberta a pasta onde o jogo foi instalado, abra a pasta "mods".<br />
Baixe o ComputerCraft 1.7.9 e coloque dentro da pasta "mods".<br />
Reinicie o Tlauncher e aperte em "Entrar no jogo".<br />
<h1 display="inline">
  <img src="server/install/instalacao2.png">
</h1>
## 💻 Entrando no servidor
 Com o jogo aberto, selecione a opção "Multiplayer"<br />
 <h1 display="inline">
  <img src="server/install/instalacao3.png">
</h1>
Na nova tela pressione o botão "Adicionar Servidor"<br />
<h1 display="inline">
  <img src="server/install/instalacao4.png">
</h1>
No campo Endereço do Servidor coloque o seguinte endereço: router1.dynu.net:25565<br />
Pressione o botão "Concluído"<br />
<h1 display="inline">
  <img src="server/install/instalacao5.png">
</h1>
Selecione o servidor e pressione o botão entrar no Servidor.<br />
<h1 display="inline">
  <img src="server/install/instalacao6.png">
</h1>
Divirta-se.<br />


